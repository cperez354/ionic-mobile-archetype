IONIC + Angular 1 Mobile Archetype
=====================

## Getting Started

Install Ionic, cordova and gulp globally

```bash
$ npm install -g ionic cordova gulp-cli
```

Install npm devDependencies

```bash
$ npm install
```

Install bower dependencies

create .bowerrc file based off one of the included template files.

```bash
$ cp .bowerrc.noproxy .bowerrc 
```
or
```bash
$ cp .bowerrc.proxy .bowerrc 
```

```bash
$ bower install
```

