"use strict";
angular
        .module('starter', [
    
            //module core
            'ionic', //Dependencia para el funcionamiento de Ionic

            
            //module feature
            'homeModule',
            'swipeModule'
            
            //module components
        ]);