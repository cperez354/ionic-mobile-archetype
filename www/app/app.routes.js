(function () {
    "use strict";
    angular
        .module('starter')
        .config(routes);

    routes.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];

    function routes($stateProvider, $urlRouterProvider, $httpProvider) {

        $stateProvider
            .state('home', {
                url: "/",
                templateUrl: "app/feature/home/home.view.html",
                controller: "homeController as home",
            })
            .state('swipe', {
                url: "/swipe",
                templateUrl: "app/feature/swipe/swipe.view.html",
                controller: "swipeController as swipe"
            });

        $urlRouterProvider.otherwise('/');
    }

})();