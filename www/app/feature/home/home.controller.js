(function () {
    "use strict";

    angular
        .module('homeModule')
        .controller('homeController', homeController);

    //Inyeccion de dependencias
    homeController.$inject = ['$stateParams', '$state'];

    function homeController($stateParams, $state) {

        var vm = this;

        vm.swipe = swipe;

        function swipe(){
            $state.go('swipe');
        }

    }
})();